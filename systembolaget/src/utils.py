import logging
import sys
import datetime
import os
import pickle
import json

# Logger init
logger = logging.getLogger(__name__)


logger = logging.getLogger('chardet.charsetprober')
logger.setLevel(logging.INFO)

def create_logger():
    '''
    Creates a logger to log the script
    
    returns: logger object
    '''
    date_today = datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S')
    #log_file_name = "../log/{}.log".format(date_today)
    
    # Log path
    log_path = 'log'
    if not os.path.exists(log_path):
        os.makedirs(log_path, exist_ok=True)
    log_file_name = "{}/{}.log".format(log_path, date_today)

    # Create logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    
    # Create file handler and set level to debug
    fh = logging.FileHandler(log_file_name, mode = 'w')
    fh.setLevel(logging.DEBUG)
    
    # Create console handler with a higher log level
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.INFO)
    
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)8s --- %(message)s ' +
                                  '(%(filename)s:%(lineno)s)',datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # Add the handlers to the logger
    root_logger.addHandler(fh)
    root_logger.addHandler(ch)
    
    return logger


def check_exists(file_path):
    '''
    Checks if a file exists

    Return: 0 if not exists, else 1
    ''' 
    if not os.path.exists(file_path):

        return 0

    return 1


def write_pickle(file_path, data, msg="written to disk"):
    '''
    Dumps data given a file_path

    Inputs
    -----------
    * file_path : str
        Where to store the file

    * data : Any
        What data to pickle
    
    * msg : str [default="written to disk"]
        Debug logging message
    '''

    logging.debug("{} {}!".format(file_path, msg))
    pickle.dump(data, open(file_path, 'wb'))


def load_pickle(file_path, msg="read from disk"):
    '''
    Reads a pickle file given a file_path

    Inputs
    -----------
    * file_path : str
        Where to store the file
    
    * msg : str [default="written to disk"]
        Debug logging message

    Return
    ----------
    Expport_data (dict) with read pickle file contents
    '''

    # Check if file path exists
    if os.path.exists(file_path) == True:
        try:
            data = pickle.load(open(file_path, 'rb'))
            logging.debug("{} {}!".format(file_path, msg))
            return data

        except Exception as e:
            logger.warning(f"Error opening pickle file: {file_path} | Error: {e}")
    
    # Else raise error
    else:
        logging.info(f"File: {file_path} does not exist!")
    


def read_txt_file(file_path):
    '''
    Reads a txt file and appends each line to a list which it returns

    Attributes:

        * file_path (str): What file to read

    return: List with all lines (list)
    '''

    export_list = []

    with open(file_path, 'r', encoding="utf-8") as f:

        temp = [line.strip() for line in f.readlines()]

        for line in temp:

            export_list.append(line)

    return export_list


def write_json(data, file_path, logger_msg="Data written to disk"):
    '''
    Writes JSON data to disk

    Attributes:
        * data (ANY): Data that will be written to file
        * file_path (str): Path where to store the data
        * logger_msg (str): Logger debug message
    '''

    # Open the file
    with open(file_path, 'w', encoding='utf-8') as outfile:
        
        # Writes the file as JSON
        json.dump(data, outfile, indent=4, 
                  ensure_ascii=False)
        logging.debug(logger_msg)


def write_csv(data, file_path, logger_msg="Data written to disk"):


    with open(file_path, 'w', encoding='utf-8') as outfile:

        outfile.write(data['legend'])
        # Writes the file as JSON
        outfile.write(data['data'])
        logging.info(logger_msg)



def load_json(file_path):
    '''
    Loads JSON data from disk to memory

    Attributes:
        * file_path (str): Path where the file is located

    return: Loaded data as JSON 
    '''

    if check_exists(file_path) == 1:

        with open(file_path, 'r', encoding='utf-8') as json_file:

            data = json.load(json_file)

            return data

    return "FILE_DOES_NOT_EXISTS"