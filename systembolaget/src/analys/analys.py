import os
import logging

import pandas as pd

from src import utils
from src import init
from src.analys import fetcher


logger = logging.getLogger(__name__)


def get_sortiment(*args, **kwargs):
    '''
    Gets the sortiment from systembolag

    Parameters
    ----------
    base_path : str
        Origin of the project

    overwrite : {True, False}, optional
        Should the data be overwritten?

    Returns
    -------
    sortiment : pandas.dateframe
        The sortiment as a pandas dataframe
    '''

    base_path = "."
    overwrite = False
    number_of_items = 20
    write_csv = False

    if "base_path" in kwargs:
        base_path = kwargs["base_path"]
        print(base_path)
    if "overwrite" in kwargs:
        overwrite = kwargs["overwrite"]

    # Creates the folder structure
    init.create_structure(base_path)
    # Create project specific folders
    folders = ['output/responses/sortiment', 'output/sortiment']
    init.check_project_folders(base_path, folders)

    # Output path
    output_path = "{}/output".format(base_path)

    # Assign the file path
    sortiment_file_path = "{}/responses/sortiment/sortiment.pickle".format(output_path)
    
    # Download the sortiment
    if utils.check_exists(sortiment_file_path) == 0 or overwrite is True:
        sortiment = fetcher.download_sortiment()
        # Save the data
        utils.write_pickle(sortiment_file_path, sortiment)
    
    # Read the pickled data
    else:
        sortiment = utils.load_pickle(sortiment_file_path)
 

    # Lets analyzse it
    # TODO: Fix the inputs at get_sortiment()
    sortiment = apk_analys(sortiment, False, "apk", 20, True)


    return sortiment


def apk_analys(sortiment, verbose=True, sort_by="apk", n=5, write_csv=False):
    '''
    Adds the APK and kr/alkohol to the dataframe for "anlysis"

    Parameters
    ----------
    sortiment : pandas.dateframe
        Sortiment as a dataframe
    
    verbose : {True, False}, optional
        Should the best APK be printed out

    sort_by : {"apk"}, optional 
        What to sort by

    n : int {>1}, optional 
        How many items to show

    write_csv : {True, False}, optional 
        Should the top result be saved to file?

    Returns
    -------
    sortiment: pandas.dateframe
        Sortiment as pandas dataFrame
    '''
    
    # Ignore pandas error
    with pd.option_context('mode.chained_assignment', None):
        # Parse the alcohol volume to alcohol content 
        sortiment['alkohol_volym'] = sortiment['Alkoholhalt'].str.replace(r'%', '').astype(float) / 100
        sortiment = sortiment.query('alkohol_volym != inf') # Remove items with no alchohol
        sortiment = sortiment.query('alkohol_volym != 0')

        # Calculate APK
        sortiment['apk'] = (sortiment['alkohol_volym'] * sortiment['Volymiml']) / sortiment['Prisinklmoms']

        # Calculate Kr/alkohol
        sortiment['kr_per_alk'] = sortiment['alkohol_volym'] / sortiment['Prisinklmoms']

    # Do nice print
    if verbose == True:
        # Sort the array by a criteria and show n items
        sorted_apk_sortiment = sortiment.sort_values(
            by=[sort_by], ascending=False).head(n)
        top_apk = sorted_apk_sortiment[
            ['Varnummer', 'Namn', 'Namn2', 'Alkoholhalt', 
            'Volymiml', 'Typ', 'Prisinklmoms', 'apk', 'kr_per_alk']
            ]

        # Print the top APK
        print(top_apk)

        # Should the top result be written to file 
        if write_csv is True:

            top_apk.to_csv('output/sortiment/apk.csv', sep=',', index=False)


    return sortiment