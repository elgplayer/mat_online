
import logging

import requests
import pandas as pd

from src import utils

logger = logging.getLogger(__name__)


def download_sortiment():
    '''
    Download systembolagets sortiment as a pandas dataframe

    Returns
    -------
    Systembolagets sortiment as a pandas dataframe 
    '''
    
    # Request the sortiment
    url = "https://www.systembolaget.se/api/assortment/products/xls"
    logging.info("Downloading sortiment from URL: {}".format(url))
    sortiment_text = requests.get(url).text

    # Read the data with pandas
    sortiment_dataframe = pd.read_html(sortiment_text, header=0)[0]
    logging.debug("Converting the sortiment to a Dataframe")


    return sortiment_dataframe