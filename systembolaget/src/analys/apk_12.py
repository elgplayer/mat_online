# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 23:21:37 2020

@author: Carl
"""


import math
import pandas as pd

sortiment_size = len(sortiment)
sortiment_size_ceil = math.floor(sortiment_size / 10)

sortiment.sort_values(by=['apk'], ascending=False)["apk"][::sortiment_size_ceil]

sortiment_size_ceil = math.floor(sortiment_size / 12)

sortiment.sort_values(by=['apk'], ascending=False)["apk"][::sortiment_size_ceil]