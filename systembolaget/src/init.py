import os
import logging

from src import utils
from src.wine_inventory import fetcher
from src.wine_inventory import wine_parser

logger = logging.getLogger(__name__)


def create_structure(base_path=""):
    '''
    Create necessary folders

    Parameters
    ---------- 
    base_path : str
        Origin of the project
    '''    

    base_folders = ['output', 'deb', 'log', 'output']

    for folder in base_folders:
        base_folder_path = "{}/{}".format(base_path, folder)
        # Create the folder if it does not exists
        if utils.check_exists(base_folder_path) == 0:
            logging.info("Creating folder: {}".format(base_folder_path))
            os.makedirs(base_folder_path, exist_ok=True)

    



def check_project_folders(base_path="", folders=[]):
    '''
    Create project specific folders

    Parameters
    ----------
    base_path : str
        Origin of the project
    
    folders : list
        List with folders to create
    
    '''
    for folder in folders:

        if base_path != "":
            folder_path = "{}/{}".format(base_path, folder)
        else:
            folder_path = folder

        # Create the folder if it does not exists
        if utils.check_exists(folder_path) == 0:

            logging.info("Creating folder: {}".format(folder_path))
            os.makedirs(folder_path, exist_ok=True)       