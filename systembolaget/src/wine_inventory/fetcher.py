# -*- coding: utf-8 -*-
"""
Created on Sun Aug 25 17:09:46 2019

@author: Carl
"""

import os
import time
import json
import pickle
import logging
import datetime
import sys

import requests
import pandas as pd

from src import utils
from src.wine_inventory import wine_parser

# Logger init
logger = logging.getLogger(__name__)


def get_all_stores():
    '''
    Gets all systembolaget store information

    Returns
    -------
    response_json : dict
        Info about all the stores
    '''
    url = "https://www.systembolaget.se/api/sitesearch/typeahead"

    querystring = {"searchquery":""}

    headers = {
        'access-control-allow-origin': "*.hotjar.com",
        'accept': "application/json, text/plain, */*",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
        'cache-control': "no-cache"
        }

    response = requests.request("GET", url, headers=headers, params=querystring)
    response_json = response.json()


    return response_json
        

def get_store_info(site_id):
    '''
    Gets store info

    Parameters
    ----------
    site_id : str
        Unique id for each store, e.g: "0212"

    Returns
    -------
    response_json : dict
        Store info
    '''

    logging.debug("Getting info for store: {}".format(site_id))
    
    url = "https://www.systembolaget.se/api/site/getsites"
    querystring = {"siteIds": site_id}

    headers = {
        'access-control-allow-origin': "*.hotjar.com",
        'accept': "application/json, text/plain, */*",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'sec-fetch-mode': "cors",
        'cache-control': "no-cache",
        }

    response = requests.get(url, headers=headers, params=querystring)
    response_json = response.json()[0] # The json is a list


    return response_json


def get_stock_balance(product_id, sites_id):
    '''
    Gets the current stock balance of a product given all store id's

    Parameters
    ----------
    product_id : str
        Unique SYSTEMBOLAGET product id, e.g: "3555521"
    
    sites_id : str, list
        What stores to check the balance, e.g: "0212" (store id [can be comma separted]): 
       
    Returns
    -------
    response_json : dict
        Stock balance of the product
    '''

    logging.debug("Getting stock balance for product: {} in store: {}".format(product_id, sites_id))
    
    url = "https://www.systembolaget.se/api/product/getstockbalance"
    payload = str({"productId": product_id, "siteIds":[sites_id]})
    payload = payload.replace("'", '"')

    headers = {
        'sec-fetch-mode': "cors",
        'dnt': "1",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
        'content-type': "application/json;charset=UTF-8",
        'access-control-allow-origin': "*.hotjar.com",
        'accept': "application/json, text/plain, */*",
        'x-requested-with': "XMLHttpRequest",
        'cache-control': "no-cache",
        }

 
    response = requests.request("POST", url, data=payload, headers=headers)
    response_json = response.json()


    return response_json


def get_product_gtin(output_path, gtin, ovewrite=False):
    '''
    Searches for product by EAN number

    Parameters
    ----------
    output_path : str
        Where to store the data
    
    gtin : str, int
        EAN number to search for. e.g: 06001497404566
       
    Returns
    -------
    response_json : dict, str
        Article info (dict) or "NO_PRODUCT" (str)
    '''

    return_data = None
    file_path = "{}/responses/gtin/{}.pickle".format(output_path, gtin)

    if ovewrite == True or utils.check_exists(file_path) == 0:
        logging.debug("Fetching data about GTIN: {}".format(gtin))
        url = "https://api-extern.systembolaget.se/product-open/v1/GetProductByGtin"
        querystring = {"gtin": gtin}
        headers = {
            'ocp-apim-subscription-key': "cd47c7efba6247cca3a49a29a23b1b9a",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Host': "api-extern.systembolaget.se",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
            }
        
        response = requests.request("GET", url, headers=headers, params=querystring)
        
        # Save the responses
        utils.picke_dump(file_path, response)

    else:

        logging.debug("GTIN pickle read from file: {}".format(gtin))
        response = utils.pickle_read(file_path)
    
    # Check if the product is found
    response_text = response.text
    
    # Check for if there are products
    if response_text == "No product found.":
        return_data =  "NO_PRODUCT"
    # Parse the response as JSON
    else:
        return_data = response.json()


    return return_data


def get_product_info(product_number):
    '''
    Gets product info by product number

    Parameters
    ----------
    product_number : str
        Product number

    Returns
    -------
    response_json: dict
        Product information as JSON (dict)
    '''

    url = "https://www.systembolaget.se/api/product/GetProductsForAnalytics"

    # Parses the payload
    payload = str({"ProductNumbers": [product_number]})
    payload = payload.replace("'", '"')

    headers = {
        'Sec-Fetch-Mode': "cors",
        'DNT': "1",
        'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
        'Content-Type': "application/json;charset=UTF-8",
        'Access-Control-Allow-Origin': "*.hotjar.com",
        'Accept': "application/json, text/plain, */*",
        'X-Requested-With': "XMLHttpRequest",
        'Cache-Control': "no-cache",
        'Host': "www.systembolaget.se",
        #'Cookie': "lbsession=rd10o00000000000000000000ffff91fbfd06o80",
        'Accept-Encoding': "gzip, deflate",
        'Content-Length': "29",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }

    response = requests.request("POST", url, data=payload, headers=headers)
    response_json = response.json()

    return response_json


def search_sortiment(searchquery, site_id):
    '''
    Searches the sortiment

    Parameters
    ----------
    searchquery : str
        Search query, e.g "Pinot noir"

    site_id : str, list
        What stores to check the balance, e.g: "0212" (store id [can be comma separted]): 

    Returns
    -------
    # TODO
    # WHAT???
    '''

    url = "https://www.systembolaget.se/api/productsearch/search/sok-dryck/"
    querystring = {"searchquery": searchquery,"sortdirection":"Ascending","site": site_id,"fullassortment":"1"}
    headers = {
        'access-control-allow-origin': "*.hotjar.com",
        'accept': "application/json, text/plain, */*",
        'dnt': "1",
        'x-requested-with': "XMLHttpRequest",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
        'cache-control': "no-cache"
        }

    response = requests.request("GET", url, headers=headers, params=querystring)
    response_json = response.json()

    return response_json


def download_product_picture(output_path, gtin, url, overwrite=False):
    '''
    Downloads the picture of a product and saves it to disk

    Parameters
    ----------
    output_path : str
        Where the project is oriented from

    gtin : str, int
        EAN number to search for. e.g: 06001497404566

    url : str
        Where should the picture be downloaded from

    overwrite : {True, False}, optional
        Should the picture be overwritten?

    Returns
    -------
    local_file_path : str
        Local URL to the picture
    '''

    file_extension = wine_parser.get_product_extension(url)
    file_path = f"{output_path}/responses/product_picture/{gtin}.{file_extension}"

    # The local URL file_path
    local_file_path = os.getcwd() + file_path

    if utils.check_exists(file_path) == 0 or overwrite == True:

        response = requests.request("GET", url)
        logging.debug("Downloading product_picture... GTIN: {} | PRODUCT_URL: {}".format(gtin, url))

        if response.status_code == 200:
            # Write the file to disk
            with open(file_path, 'wb') as f:
                for chunk in response:
                    f.write(chunk)
        else:
            logging.info(f"Response status code: {response.status_code}")


    return local_file_path


def download_sortiment(output_file):
    '''
    Download sortiment

    Parameters
    ----------
    output_path : str
        Where the project is oriented from

    Returns
    -------
    data : pandas.dateframe
        Sortiment
    '''
    
    # Request the sortiment
    sortiment_url = "https://www.systembolaget.se/api/assortment/products/xls"
    sortiment = requests.get(sortiment_url).text
    
    # Read the data with pandas
    data = pd.read_html(sortiment, header=0)[0]
    pickle.dump(data, open('../output/sortiment.pkl', 'wb'))
    

    return data
