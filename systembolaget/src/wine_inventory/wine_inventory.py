import os
import json
import logging

from src import utils
from src import init
from src.wine_inventory import wine_parser
from src.wine_inventory import fetcher


# Logger init
logger = logging.getLogger(__name__)


def inventory_status(base_path, wine_list_file="wine_list.txt"):
    '''
    Creates a list of the wine inventory with the appropriate info from systembolaget

    Attributes:

        * deb_path (str): Where the deb/ folder is located

    return: debug_val
    '''

    inventory_folders = ['output/responses/gtin', 'output/responses/product_picture', 'output/inventory']

    # Create the necarry folder structure for the prgoram
    init.check_project_folders(base_path, inventory_folders)

    # TODO: This looks awful
    deb_path = "{}/deb".format(base_path)
    output_path = "{}/output".format(base_path)
    # Remove the first char
    if base_path == "":
        deb_path = deb_path[1:]
        output_path = output_path[1:]

    store_id_file = "shops_id.txt"
    shops_json = "shops.json"

    wine_list_file_path = "{}/{}".format(deb_path, wine_list_file)
    store_id_file_path = "{}/{}".format(deb_path, store_id_file)
    shop_json_path = "{}/{}".format(deb_path, shops_json)

    # Create the store_info_dict
    store_info_dict = wine_parser.create_store_dict(store_id_file_path, shop_json_path)

    # Check if the file exists
    if utils.check_exists(wine_list_file_path) == 0:

        raise "NO_WINE_LIST"

    # Reads the wine list
    wine_list = utils.read_txt_file(wine_list_file_path)

    # Converts the list to a dict
    wine_dict = wine_parser.wine_list_to_dict(wine_list)
    
    legend = ""
    legend_created = 0
    print_str = ""

    # Goes through the wine_dict
    for k,x in enumerate(wine_dict):
        
        # Get info about the product by fetching data from it's GTIN api
        selected_wine = wine_dict[x]
        wine_dict[x]['product_gtin'] = fetcher.get_product_gtin(output_path, selected_wine['gtin'])

        # Check that the wine actually exists in systembolaget's database
        if selected_wine['product_gtin'] != "NO_PRODUCT":

            # Abbrivations
            product_data = selected_wine['product_gtin']
            gtin = selected_wine['gtin']
            count = selected_wine['count']
            product_url = selected_wine['product_gtin']['ProductImage']['ImageUrl']
            wine_dict[x]['picture_exists'] = 0

            # Create a dictionar where only the "interesting" criterias are in it
            wine_str = wine_parser.create_wine_string(product_data, count)
            wine_dict[x]['wine_str_categories'] = wine_str

            if product_url != "":

                wine_dict[x]['product_url'] = fetcher.download_product_picture(output_path, gtin, product_url)
                wine_dict[x]['picture_exists'] = 1

            for kx, ky in enumerate(wine_str):

                if legend_created == 0:

                    legend += "{}|".format(ky)

                print_str += "{}|".format(wine_str[ky])

            print_str += "\n"

            if legend_created == 0:

                legend += "\n"
                legend_created = 1


    write_dict = {'data': print_str, 'legend': legend}
    utils.write_csv(write_dict, "output/inventory/current_inventory.csv", "Inventory CSV written to disk")

    
    # Create a export variable
    export_dict = wine_dict

    return export_dict


