import os
import pickle
import json
import logging

from src import utils
from src.wine_inventory import fetcher

# Create logger
logger = logging.getLogger(__name__)


def signal_handler(signal, frame):

    global interrupted
    interrupted = True

#signal.signal(signal.SIGINT, signal_handler)

# TODO: Make the list an argument
def load_site_id():
    '''
    Reads the file "butiker_site.txt" and returns a list by all stores siteIds
    
    Returns
    -------
    Site_id_list : list
        List with all store Ids
    '''

    site_id_list = []
    site_id_path = '../deb/butiker_site.txt'

    if utils.check_exists(site_id_path) == 0:
        raise "NO SITE IDS"

    with open(site_id_path, 'r', encoding='utf-8') as f:
        lines = f.read().splitlines()

        for site_id in lines: 
            site_id_list.append(site_id)


    return site_id_list
    

def load_store_info():
    '''
    Loads the store info JSON file
    Either from reading the local file or by downloading the file if new stores 
    have been detected or if the file does not exists
    
    Returns
    -------
    store_info_list : list
        Store info list
    
    '''
    store_info_path = '../deb/store_info.json'
    download_store_info = 0
    
    store_info_list = []
    site_id_list = load_site_id()

    # If the list does not exists
    if utils.check_exists(store_info_path) == 0:
        logging.info("Store list does not exists...")
        download_store_info = 1
    # Load the list
    else:
        store_info_list = utils.load_json(store_info_path)
        # Check that the store info list is the same length
        if len(site_id_list) <= len(store_info_list):
            download_store_info = 1
        
    # If store INFO should be downloaded
    if download_store_info == 1:
        # Goes through all site ids and get the info about them
        for site_id in site_id_list:
            # Appends the store info to a list
            store_info_list.append(fetcher.get_store_info(site_id))
        # Save the list as JSON
        utils.write_json(store_info_list, store_info_path)

    
    return store_info_list
