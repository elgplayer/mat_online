
import os
import json
import logging

from src import utils
from src.wine_inventory import fetcher

# Logger init
logger = logging.getLogger(__name__)


def parse_gtin(ean):
    '''
    Parses EAN to GTIN number

    Parameters
    ----------
    ean : str,int
        EAN number to search for. e.g: 6001497404566

    Returns
    -------
    gtin : str, int
        GTIN number
    '''
    
    # Convert the GTIN to string
    gtin = str(ean)
    return_data = None

    # If the length is over 14 characters
    if len(gtin) > 14:
        return_data =  gtin[len(gtin) - 14:len(gtin)]
    else: 
        # If the length is under 14
        while (len(gtin)) < 14:
            tmp_gtin = "0" + gtin
            gtin = tmp_gtin
        return_data = gtin

        
    return return_data


def create_store_dict(store_id_file_path, shop_json_path, overwrite=False):
    '''
    Creates a store dict and returns it

    Parameters
    ----------
    store_id_file_path : str
        Where the store_id file is located
    
    shops_json_path : str
        Where the shop_json file is
    
    ovewrite : {True, False}
        Should the file be overwritten

    Returns
    -------
    store_info_dict : dict
        Store_dict # TODO: ???
    '''

    # Check if the file exists
    if utils.check_exists(store_id_file_path) == 0:

        logging.info("NO_STORE_PATH: {}".format(store_id_file_path))
        raise "NO_STORE_PATH"

    # Lets load from file if the file exits
    if overwrite == False:
        if utils.check_exists(shop_json_path) == 1:
            logging.debug("Store dict loaded from file: {}".format(shop_json_path))
            # Try to parse as JSON; if fail --> Init scrape
            # TODO: try, except bajs
            try:
                store_info_dict = utils.load_json(shop_json_path)
                return store_info_dict
            except:
                pass
            
    # Loads the store id as a list
    store_ids = utils.read_txt_file(store_id_file_path)
    store_info_dict = {}

    # Goes through the store_id list and fetches all info about it
    for k,x in enumerate(store_ids):

        store_info = fetcher.get_store_info(store_ids[k])
        tmp = {
            'alias': store_info['Alias'], 
            'city': store_info['City'], 
            'name': store_info['Name']
        }
        store_info_dict[store_ids[k]] = tmp

    # Writes the data as json
    utils.write_json(store_info_dict, shop_json_path)


    return store_info_dict


def wine_list_to_dict(wine_list):
    '''
    Take the wine_list and counts how many of each 
    item there is and returns it as a dict. It also parses
    the ean to gtin

    Parameters
    ----------
    wine_list : list
        Wine_list to count

    Returns
    -------
    wine_dict : dict
        Dictionary with the counted items
    '''

    wine_dict = {}
    
    for wine_ean in wine_list:
        if wine_ean not in wine_dict:
            gtin = parse_gtin(wine_ean)
            tmp = {
                'ean': wine_ean, 
                'gtin': gtin, 
                'count': 1
            }
            wine_dict[wine_ean] = tmp           
        else: 
            wine_dict[wine_ean]['count'] += 1


    return wine_dict
        

def create_wine_string(product_gtin, count=0):
    '''
    Creates a new dictionary where only the selected criterias are present

    Parameters
    ----------
    product_gtin : dict
        Diciontary with all information about the product

    count : int
        Counts how many of each product

    Returns
    -------
    new_dict : dict
        New, smaller dictionary
    '''
    
    # Interest
    categories_of_interest = {
        'name': 'ProductNameBold', 
        'short_name': 'ProductNameThin',
        'category': 'Category',
        'sub_category': 'SubCategory',
        'year': 'Vintage',
        'country': 'Country',
        'origin_1': 'OriginLevel1',
        'origin_2': 'OriginLevel2',
        'alcohol_percentage': 'AlcoholPercentage', 
        'price': 'Price',
        'taste': 'Taste', 
        #'short_description': 'BeverageDescriptionShort', 
        'usage' :'Usage', 
        'volume': 'Volume'
    }

    # Taste clocks
    taste_clock_keys = {
        'taste_bitter': 'TasteClockBitter',
        'taste_acid': 'TasteClockFruitacid',
        'taste_body': 'TasteClockBody',
        'taste_roughness': 'TasteClockRoughness',
        'taste_sweetness': 'TasteClockSweetness',
        'taste_casque': 'TasteClockCasque',
        'taste_smoke': 'TasteClockSmokiness',
        'symbols': 'TasteSymbols'
    }

    new_dict = {}
    remove_none = 0 # Should None be removed?
    new_dict['count'] = str(count)

    # TODO: This looks extremly cancerous
    # Interest
    for k,x in enumerate(categories_of_interest):
        # Check that the keys is actually in the data
        if categories_of_interest[x] in product_gtin:
            if remove_none == 1:
                if product_gtin[categories_of_interest[x]] != None:
                    new_dict[x] = product_gtin[categories_of_interest[x]]
            else: 
                new_dict[x] = product_gtin[categories_of_interest[x]]

    # Taste clocks
    for k,x in enumerate(taste_clock_keys):
        # Check that the clock is actually in the data
        if taste_clock_keys[x] in product_gtin:
            # Check that the taste symbol is not empty
            if remove_none == 1:
                if product_gtin[taste_clock_keys[x]] != None:
                    new_dict[x] = product_gtin[taste_clock_keys[x]]
            else:
                 new_dict[x] = product_gtin[taste_clock_keys[x]]

    
    return new_dict


def get_product_extension(url):
    '''
    Gets the extension of the file name from a given URL

    Parameters
    ----------
    url : str
        URL

    Returns
    -------
    File extension of the attachment the URL is pointing towards
    '''

    return url.split(".")[-1]