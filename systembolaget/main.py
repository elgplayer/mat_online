# -*- coding: utf-8 -*-
"""
Created on Sat Aug 24 15:14:56 2019

@author: Carl
"""

import os
import json
import time
import datetime
import sys
import logging
import pickle

from src import utils
from src import init
from src.wine_inventory import wine_inventory
from src.analys import analys


# Logger init
logger = logging.getLogger(__name__)
logger = utils.create_logger()
logging.info("Starting")




# Gets the sortiment top apk
sortiment = analys.get_sortiment(base_path=".", overwrite=False, sort_by="apk")

# Run the wine_inventory status function
# wine_list = "sprit.txt"
# debug_val = wine_inventory.inventory_status(base_path, wine_list)
