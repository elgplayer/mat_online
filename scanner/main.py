# -*- coding: utf-8 -*-

import os
import json
import pickle
import sys
import logging


import numpy as np
import pandas as pd


from src import utils
from src import export_data
# from src.gsheets import df_to_gsheets

# Create logger
logger = utils.create_logger()

# Get the loggers name
logger = logging.getLogger(__name__)
logging.info("Started!")

# IN and OUT files
input_file = "input/skann_garage.txt"
output_file = "output/test.csv"

# Load the SKU DB
sku_db_path = "../ica/output/sku_db.pkl"
sku_db = utils.load_pickle(sku_db_path)["data"]

# Create the inventory list
export_data.create_inventory_list(sku_db, input_file, output_file)

logging.info("Finished!")