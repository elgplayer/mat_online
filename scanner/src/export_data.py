import os
import json
import logging

import pandas as pd


from src.gsheets import df_to_gsheets
from src import utils

# Get the loggers name
logger = logging.getLogger(__name__)


def load_file(file_path):
    '''
    Loads a file

    Input
    ---------
    * file_path : str
        File to load

    Return
    ---------
    Returns the file data
    '''

    # Check if the file exist
    if not os.path.exists(file_path):
        raise "FILE_DOES_NOT_EXIST"

    with open(file_path, "r") as f:
        # Read the lines in the file and remove the new line char
        data = [x.strip() for x in f.readlines()]
        return data


def select_details(sku_db, df, sku):
    '''
    Takes an product and appends the intresting data to an Dataframe

    Input
    ---------
    * item : dict
        Item from the SKU database
    
    * df : DataFrame
        Dataframe to append the dictionary to

    Return
    ---------
    Returns a dataframe with the intresting data appended to it
    '''

    

    # Abbrivation
    try:
        item = sku_db[sku]
        product = item["product"]
        # sku = product["sku"]
        name = product["name"]
        item_type = product["type"]
        brand = product["brand"]
        weight = product["netWeightVolume"]

        list_price = item["price"]["listPrice"]
        comp_price = item["price"]["comparePrice"]
    except:
        # sku = product["sku"]
        name = ""
        item_type = ""
        brand = ""
        weight = ""

        list_price = ""
        comp_price = ""        


    data_dict = {
        'name': name,
        'item_type': item_type,
        'list_price': list_price,
        'comp_price': comp_price,
        'brand': brand,
        'weight': weight,
        'sku': sku
    }

    # Add the data dict to the dataframe
    df.loc[len(df)] = data_dict


    return df


def create_inventory_list(sku_db, input_file, output_file="output/inventory.txt",
                          sheets_export=True, csv_export=True):
    '''
    Creates a list of a given inventory file

    Input
    ---------
    * sku_db : Dict
        SKU database
    
    * input_file : str
        File path to a file with barcodes to search for codes for

    * output_file : str [default="output/inventory.txt"]
        Output file
    
    * sheets_export : boolean [default=True]
        Should the data be exported to GOOGLE sheets?
    
    * csv_export : boolean [default=True]
        Should the data be exported to disk as a CSV?

    '''

    # Load the input file
    data_file = load_file(input_file)

    # Init the emtpy Dataframe
    col_names = ['name', 'item_type', 'list_price', 'comp_price', 'brand',
                 'weight', 'sku']
    df = pd.DataFrame(columns=col_names)

    # Iterate over the data file
    counter = 0
    for sku in data_file:
    
            # if sku in sku_db:
        counter += 1
        df = select_details(sku_db, df, sku)

        # if sku in sku_db:
        #     counter += 1
        #     df = select_details(sku_db[sku], df, sku)

        # else:
        #     print("Not found: ", sku)

    # Print how many products that were found in the SKU database
    logging.info("Products found: {} / {}".format(counter, len(data_file)))

    # Write to file
    if csv_export is True:
        try:
            df.to_csv(output_file, index=False)
        except Exception as e:
            logging.error(f"CSV export error | Error: {e}")
            logging.info("Skipping CSV export...")
            pass
    
    # Spreedsheet export
    if sheets_export is True:
        df_to_gsheets.export_Data_To_Sheets(df, "garage!A1")
