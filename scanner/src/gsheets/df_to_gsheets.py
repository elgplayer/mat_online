from src.gsheets.google import Create_Service
import pandas as pd
import numpy as np


CLIENT_SECRET_FILE = 'deb/credentials.json'
API_SERVICE_NAME = 'sheets'
API_VERSION = 'v4'
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
gsheetId = '1sJbQ3jFNocORqJElpuZHmQOA8WWGO_O99bn1JNlqOCs'

service = Create_Service(CLIENT_SECRET_FILE, API_SERVICE_NAME, API_VERSION, SCOPES)

def export_Data_To_Sheets(df, sheet_range='Sheet1!A1'):
    '''
    Does the actuall export of data to google sheets

    Input
    ---------
    * df : DataFrame
        Dataframe to insert at the spreedsheet    

    * sheet_range : str [defaualt=Sheet1!A1]
        Where in the sheet should the data write start?
    '''

    # Send the command to GOOGLE with the data
    response_date = service.spreadsheets().values().update(
        spreadsheetId=gsheetId,
        valueInputOption='RAW',
        range=sheet_range,
        body=dict(
            majorDimension='ROWS',
            values=df.T.reset_index().T.values.tolist())
    ).execute()