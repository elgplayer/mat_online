# -*- coding: utf-8 -*-

import os
import json
import pickle
import sys
import logging
import asyncio
from concurrent.futures import ThreadPoolExecutor

import requests

from src import utils
from src import fetcher
from src import scraper
from src import database_func


# Create logger
logger = utils.create_logger()
# Get the loggers name
logger = logging.getLogger(__name__)
logger.info("Starting program!")

# Generate a weekly path
date = database_func.get_date()
#path = "output/{}/{}/dependencies".format(date['year'], date['week'])
path = "output/db_out"

# Fetch the inventory
overwrite = True
database = fetcher.get_inventory(path, overwrite)

logging.info("Program done!")
