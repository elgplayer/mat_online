import base64
import logging

from src import utils

# Get the loggers name
logger = logging.getLogger(__name__)


def str_to_base64(string):
    '''
    Takes a string and converts encodes is as base64 (URL safe)

    Inputs
    -----------
    * string : str
        String to encode

    Return
    ---------
    Base64 encoded string
    '''
    # Convert string to byte object
    string_bin = string.encode('utf-8') 

    # Encode the byte object as base_64
    picture_name_encoded = base64.urlsafe_b64encode(string_bin).decode("utf-8")

    return picture_name_encoded


def base64_to_str(base64_str):
    '''
    Takes a string (base64) and decodes it to a normal string

    Inputs
    -----------
    * string : str
        String to decode

    Return
    ---------
    Normal string
    '''
    return base64.urlsafe_b64decode(base64_str).decode("utf-8")


def get_product_code(product_dict):
    '''
    Gets a products product code

    Inputs
    -----------
    * product_dict : dict
        Dictionary with product data

    Return
    -----------
    Product code (string)
    '''
    # Get the product code
    try:
        product_code = product_dict['code']
       
    except Exception as e:
        logging.info("ERROR: {} | {}".format(e, product_dict))
        product_code =  "CODE_PARSING_ERROR"


    return product_code


def parse_producs_status(path, product_list, overwrite=False):
    '''
    Reads the product data from disk given product_codes from a product_list

    Inputs
    -----------
    * path : str
        Path where to store the data

    * product_list : list
        List with all products

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    List with all path_urls
    '''
    products = []
    file_path = f"{path}/complete_product_list.pkl"

    # Read from disk
    if overwrite == False and utils.check_exists(file_path) == 1:
        
        # Try to read the data from disk
        products = utils.pickle_read(file_path, "Products_file")
        if products['status'] == 1:
            products = products['data']
            logging.info("Reading products file!")
            return products
        else:
            logging.info(f"Products error: {products['msg']} --> Trying to scrape data")

    # Go through every product in the product_list
    for product in product_list:

        if product['status'] == 1:

            products.append(product['data'])

        else:

            logging.debug(f"Product error: {product['msg']}")

    if len(products) != 0:

        utils.pickle_dump(file_path, products, "Products_file") 
        return products


    return products