# -*- coding: utf-8 -*-

import json
import os
import logging
import base64
import shutil

import requests
import hashlib 

from src import utils
from src import database_func
from src import string_parser

# Logger init
logger = logging.getLogger(__name__)



def get_path_urls(path, overwrite=False):
    '''
    Get all path_urls

    Inputs
    -----------
    * path : str
        Path where to store the data

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    List with all path_urls
    '''

    file_name = f"{path}/path_urls.pkl"

    # Read data from file if it exists
    if overwrite == False and utils.check_exists(file_name) == 1:
        
        path_urls = utils.pickle_read(file_name, "Path_URLS")

        if path_urls['status'] == 1:
            path_urls = path_urls['data']
            return path_urls
        else:
            logging.info(f"Path_urls error: {path_urls['msg']} --> \
                Trying to scrape data")    
    
    
    # Else, scrape the data
    else:

        url = "https://www.willys.se/leftMenu/categorytree"
        logging.debug("Fetching path_urls: {}".format(url))
        data = requests.get(url).json()
        path_urls = []

        for k,x in enumerate(data['children']):
            
            # Skip borttagna-noder
            if x['url'] != "Borttagna-noder":
                path_urls.append(x['url'])

        # Save the path-urls as Pickle 
        utils.pickle_dump(file_name, path_urls, "Path_URLs")


    return path_urls


def fetch_products(path, path_url, data_size=20, overwrite=False):
    '''
    Fetches products for a given path_url

    Inputs
    ----------
    * path : str
        Path where to store the data

    * path_url : str
        Path url, e.g: "Kott-chark-och-fagel"

    * data_size : int [default=20]
        How many items of each category should be fetched?
    
    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ----------
    List with all products for a given path
    '''

    file_name = f"{path}/product_list_categories/{path_url}.pkl"
    export_data = {'status': 1, 'msg': "", "data": {}}

    # Read data from file if it exists
    if overwrite == False and utils.check_exists(file_name) == 1:
        
        data_results = utils.pickle_read(file_name, f"Product_list: {path_url}")
        if data_results['status'] == 1:
            data_results = data_results['data']
            export_data.update({'data': data_results})
            return export_data
        else:
            logging.info(f"Data_results error: {data_results['msg']} \--> \
                Trying to scrape data")

    # Else, scrape the data
    url = "https://www.willys.se/c/{}?categoryPath={}&size={}".format(
        path_url, path_url, data_size)
    data = requests.get(url).json()
    logging.debug(f"Fetching products from: {url}")

    try:
        data_results = data['results']
    except Exception as e:
        logger.debug(f"Results not in data, URL: {url} | Error: {e}")

    # Check that the data actually contains anything
    if data_results == None:
        export_data.update({'status': 0, 'msg': "NO_RESULTS"})
    else:
        # Update the data dictionary and pickle it
        export_data.update({'data': data_results})
        utils.pickle_dump(file_name, export_data, "Product_info")
        

    return export_data


def get_product_info(session, path, product_dict, overwrite=False):
    '''
    Fetches data about a specific product

    Inputs
    -----------

    * session : Request session object
        Request session object to speed up data fetching

    * path : str
        Path where to store the data

    * product_dict : dict
        Dictionary with some basic product info gathered from a path_url

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    Dictionary with all data about a product
    '''

    # Export data
    export_data = {'status': 1, 'msg': "", "data": {}}

    # Get the product code
    product_code = string_parser.get_product_code(product_dict)

    # Check for errors
    if product_code == "CODE_PARSING_ERROR":
        export_data.update({
                            'status': 0, 
                            'msg': "CODE_PARSING_ERROR"
                            })
        return export_data

    # File path
    file_path = f"{path}/products/{product_code}.pkl".format(product_code)

    # Read from disk
    if overwrite == False and utils.check_exists(file_path) == 1:

        data = utils.pickle_read(file_path, f"Product {product_code} read")
        if data['status'] == 1:
            export_data.update({'data': data['data']})
            return export_data
        else:
            logging.info(f"Product DATA error: {data['msg']} --> \
                Trying to scrape data")           

    # Scrape the data-
    url = "https://www.willys.se/axfood/rest/p/{}".format(product_code)
    response = session.get(url)
    logging.debug("Downloading product data: {}".format(product_code))

    # Check for error
    if response.status_code != 200:
        logging.info("FAILURE::{0}".format(url))
        export_data.update(
            {
                'status': 0, 
                'msg': f"RESPONSE_STATUS_CODE: {response.status_code}"
            }
        )
        return export_data

    # Parse the data as JSON
    try:
        data = response.json()
        export_data.update({'data': data})
    except Exception as e:
        export_data.update(
            {'status': 0, 'msg': "JSON_ERROR"}
        )
        return export_data

    # Parse the Ean code
    try:
        ean_code = data['ean']
    except Exception as e:
        export_data.update(
            {'status': 0, 'msg': "NO_EAN_CODE"}
        )
        #logging.info(f"Unable to get EAN code | Error: {e}")
        return export_data

    # Download the product images
    download_images(path, ean_code, data['image']) # High-res pictures
    download_images(path, ean_code, data['thumbnail']) # Thumbanil pictures

    # Save the data as PKL to disk
    utils.pickle_dump(file_path, data)


    return export_data
        

def download_images(path, ean_code, image_dict, overwrite=False):
    '''
    Downloads imgaes given a image_dict

    Inputs
    -----------
    * path : str
        Path where to store the data

    * ean_code : str
        Unique code for the product
    
    * image_dict: dict
        Dictionary that contains the URL for the image

    * overwrite : boolean [default=False]
        Should the data be overwritten?
    '''
    if image_dict == None:
        raise "NO_IMAGE_DATA"

    # Formats the picture name
    picture_format = image_dict['format']
    picture_name = f"{ean_code}_{picture_format}"
    picture_name_encoded = string_parser.str_to_base64(picture_name)

    # Sets the file name
    file_name = picture_name_encoded  + ".png"
    file_path = f"deb/products_pictures/{file_name}" # Hardcoded path

    # Download the picture if it does not exists or if it should be overwritten
    if overwrite == True or utils.check_exists(file_path) == 0:

        url = image_dict['url']

        # Add base_url to certain urls
        if '/medias/' in url:
            url = f"https://www.willys.se/{url}"
        
        # Downloads the picture
        try:  
            response = requests.get(url, stream=True)
        except Exception as e:
            logging.info("Error when fetching URL: {} | {} | \
                Error: {}".format(url, picture_name, e))
            pass

        # Save the picture as a picture on disk
        if response.status_code == 200:
            try:
                with open(file_path, 'wb') as f:
                    response.raw.decode_content = True
                    shutil.copyfileobj(response.raw, f)
            except Exception as e:
                logger.info("Error when saving picture: {} | Error: {}".format(
                    url, e))
        else:
            logging.debug("Bad response: {} | URL: {}".format(
                response.status_code, url))
        


    