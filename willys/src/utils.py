# -*- coding: utf-8 -*-

import logging
import sys
import datetime
import os
import pickle
import json
import sys


# Logger init
logger = logging.getLogger(__name__)

logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


def create_logger():
    '''
    Creates a logger object to log the program
    
    Return
    ----------
    Logger object
    '''
    date_today = datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S')

    # Log path
    log_path = 'log/'
    if not os.path.exists(log_path):
        os.makedirs(log_path, exist_ok=True)
    log_file_name = "{}/{}.log".format(log_path, date_today)
    
    # Create logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    
    # Create file handler and set level to debug
    fh = logging.FileHandler(log_file_name, mode = 'w')
    fh.setLevel(logging.DEBUG)
    
    # Create console handler with a higher log level
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.INFO)
    
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)8s --- %(message)s ' +
                                  '(%(filename)s:%(lineno)s)',datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # Add the handlers to the logger
    root_logger.addHandler(fh)
    root_logger.addHandler(ch)
    
    
    return logger


def run_from_ipython():
    '''
    Checks if the given python interpreter is running from ipython

     Return : Boolean
    ----------
    True if running from ipython, else returns False

    '''
    try:
        __IPYTHON__
        return True
    except NameError:
        return False


def pickle_dump(file_path, data, msg="written to disk"):
    '''
    Dumps data given a file_path

    Inputs
    -----------
    * file_path : str
        Where to store the file

    * data : Any
        What data to pickle
    
    * msg : str [default="written to disk"]
        Debug logging message
    '''

    logging.debug("{} {}!".format(file_path, msg))
    pickle.dump(data, open(file_path, 'wb'))


def pickle_read(file_path, msg="read from disk"):
    '''
    Reads a pickle file given a file_path

    Inputs
    -----------
    * file_path : str
        Where to store the file
    
    * msg : str [default="written to disk"]
        Debug logging message

    Return
    ----------
    Expport_data (dict) with read pickle file contents
    '''

    # Export data
    export_data = {'status': 1, 'msg': "", "data": {}}

    # Check if file path exists
    if os.path.exists(file_path) == True:

        try:
            data = pickle.load(open(file_path, 'rb'))
            logging.debug("{} {}!".format(file_path, msg))
            export_data.update({'data': data})

        except Exception as e:
            logger.warning(f"Error opening pickle file: {file_path} | Error: {e}")
    
    # Else raise error
    else:

        logging.info(f"File: {file_path} does not exist!")
        export_data.update({'status': 0, 'msg': "NO_FILE_FOUND"})
    

    return export_data


def check_exists(file_path):
    '''
    Checks if a file exists given a path

    Inputs
    ----------
    * file_path : str
        Where the file is stored

    Return
    ----------
    1 if file is found, 0 if file is not found
    '''
    if not os.path.exists(file_path):

        return 0

    return 1


def init_folder_struct(path):
    '''
    Creates all folders that are necessary

    Inputs
    -----------
    * path : str
        Path where to store the data

    '''
    folders = [
        path, 
        f"{path}/dependencies/products", 
        f"{path}/dependencies/product_list_categories",
        "deb/products_pictures"
        ]

    # Loop through the folders
    for folder in folders:

        # Check if the folder exists
        if not os.path.exists(folder):

            logging.info(f"Creating folder: {folder}")
            os.makedirs(folder, exist_ok=True)     


def write_json(file_path, data, logger_msg="Data written to disk"):
    '''
    Writes JSON data to disk

    Inputs
    ----------
    * file_path : str
        Where the file is stored
    
    * data : any
        Data that will be written as JSON
    
    * logger_msg : str
        What should be written in the log?

    '''

    # Open the file
    with open(file_path, 'w', encoding='utf-8') as outfile:
        
        try:
            # Writes the file as JSON
            json.dump(data, outfile, indent=4, 
                    ensure_ascii=False)
            logging.debug(logger_msg)
        except Exception as e:
            logging.info(f"Error: {e} when trying to save: {file_path} as json")


def load_json(file_path):
    '''
    Writes JSON data to disk

    Inputs
    ----------
    * file_path : str
        Where the file is stored

    Return
    ----------
    File contents (any)
    '''

    if check_exists(file_path) == 1:

        with open(file_path, 'r', encoding='utf-8') as json_file:

            try:
                data = json.load(json_file)
                return data
            except Exception as e:
                logging.info(f"Error: {e} when trying to open JSON file:{file_path}")

    return "FILE_DOES_NOT_EXISTS"
