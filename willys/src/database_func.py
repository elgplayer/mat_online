import datetime
import os
import logging

from src import scraper
from src import utils

# Logger init
logger = logging.getLogger(__name__)


def get_date():
    '''
    Gets the current week and year
    
    Return
    -----------
    Current year and week (dict)
    '''
    date_now = datetime.datetime.now()
    year = date_now.strftime("%Y")    
    week = date_now.strftime("%W")    


    return {'year': year, 'week': week}
    
    
def parse_product(product):
    '''
    Parses some product data to only get the essential information

    Inputs
    -----------
    * product : dict
        Dictionary with all product data

    Return
    ---------
    List with all path_urls
    '''

    try:
        data = {
            'code': product['code'],
            'ean': product['ean'],
            'comparePrice': product['comparePrice'],
            'comparePriceUnit': product['comparePriceUnit'],
            'consumerStorageInstructions': product['consumerStorageInstructions'],
            'countryOfOriginStatement': product['countryOfOriginStatement'],
            'description': product['description'],
            'displayVolume': product['displayVolume'],
            'foodContactName': product['foodContactName'],
            'manufacturer': product['manufacturer'],
            'image': product['image'],
            'ingredients': product['ingredients'],
            'maxStorageTemprature': product['maxStorageTemprature'],
            'minStorageTemprature': product['minStorageTemprature'],
            'labels': product['labels'],
            'name': product['name'],
            'nutritionDescription': product['nutritionDescription'],
            'nutritionsFactList': product['nutritionsFactList'],
            'outOfStock': product['outOfStock'],
            'online': product['online'],
            'potentialPromotions': product['potentialPromotions'],
            'price': product['price'],
            'priceNoUnit': product['priceNoUnit'],
            'priceUnit': product['priceUnit'],
            'priceValue': product['priceValue'],
            'savingsAmount': product['savingsAmount'],
            'servingSize': product['servingSize'],
            'thumbnail': product['thumbnail'],
            'ranking': product['ranking'],
            'solrSearchScore': product['solrSearchScore'],
            'tradeItemCountryOfOrigin': product['tradeItemCountryOfOrigin']
        }
    except Exception as e:
        logger.info(f"Error when trying to parse product_data | Error: {e}")
        return "PRODUCT_PARSING_ERROR"


    return data

    
def create_ean_database(path, products, overwrite=False):
    '''
    Creates an EAN database

    Inputs
    -----------
    * path : str
        Path where to store the data

    * products : list
        List with all products

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    EAN database (dict)
    '''

    ean_database = {}
    file_path = f"{path}/ean_database"

    # Iterate over the products
    for k,x in enumerate(products):
        
        try:
            ean_code = products[k]['ean']
        except Exception as e:
            logging.info("Unable to get EAN code: {}".format(e))
            pass
        
        # Abbrivation
        product = products[k]

        # Parse the product
        parsed = parse_product(product)
        
        # Init a dictionary
        ean_database[ean_code] = {'parsed_data': parsed, 'all_data': product, 'data_source': 'willys'}

    utils.write_json(f"{file_path}.json", ean_database)
    
    
    return ean_database
    
