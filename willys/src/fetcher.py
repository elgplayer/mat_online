import os
import json
import pickle
import sys
import logging
import asyncio
from requests.adapters import HTTPAdapter    
from concurrent.futures import ThreadPoolExecutor

import requests

from src import scraper
from src import utils
from src import database_func
from src import string_parser


# Logger init
logger = logging.getLogger(__name__)


def get_product_list(path, data_size=20, overwrite=False):
    '''
    Get a list with all products

    Inputs
    -----------
    * path : str
        Path where to store the data

    * data_size : int
        How many products should be fetched for the category?

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    List with all products
    '''
    product_list = []
    file_path = f"{path}/product_list.pkl"

    if overwrite == False and utils.check_exists(file_path) == 1:

        product_list = utils.pickle_read(file_path)
        if product_list['status'] == 1:
            product_list = product_list['data']
            return product_list
        else:
            logging.info(f"Product_list error: {product_list['msg']} --> Trying to scrape data")

    # Get the path urls
    path_urls = scraper.get_path_urls(path, overwrite)

    logging.info("Fetching product list for each category")
    # Iterate through the path_urls
    for k,x in enumerate(path_urls):
        
        print(f"{k} / {(len(path_urls)-1)} - {x}")
        path_url = path_urls[k]
        
        # Get the products
        products = scraper.fetch_products(path, path_url, data_size, overwrite)

        if 'status' in products:
            if products['status'] == 0:
                logger.info(f"Products {x} error: {products['msg']}")
                continue
            else:
                products = products['data']
        
        # Go through all products for a given path_url
        for product in products:
    
            # Check that the product is not None
            if product != None:
                
                # Append to the products list
                product_list.append(product)
    
    # Save the data as a pickle file
    utils.pickle_dump(file_path, product_list, "Product_list")

    logging.info("Product list Length: {}".format(len(product_list)))


    return product_list


async def get_product_data_asynchronous(path, product_list, workers=1000, overwrite=False):
    '''
    Fetches information about each product asynchronous

    Inputs
    -----------
    * path : str
        Path where to store the data

    * product_list : List
        Products to fetch

    * workers : int [default=1000]
        How many products should be fetched asynchronous?

    * overwrite : boolean [default=False]
        Should the data be overwritten?
    '''
    # Start threadpool
    with ThreadPoolExecutor(max_workers=workers) as executor:

        # Init a requests session
        with requests.Session() as session:

            session.mount('https://', HTTPAdapter(max_retries=0, pool_connections=workers, pool_maxsize=workers))
            # Set any session parameters here before calling `fetch`
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    scraper.get_product_info,
                    *(session, path, product, overwrite) # Allows us to pass in multiple arguments to `fetch`
                )
                for product in product_list
            ]
            for response in await asyncio.gather(*tasks):
                pass


async def get_data_asynchronous(path, product_list, function, workers=1000, overwrite=False):
    '''
    Downloads pictures asynchronous

    Inputs
    -----------
    * path : str
        Path where to store the data

    * product_list : List
        Products to fetch

    * workers : int [default=1000]
        How many products should be fetched asynchronous?

    * overwrite : boolean [default=False]
        Should the data be overwritten?
    '''

    export_list = []

    # Start threadpool
    with ThreadPoolExecutor(max_workers=workers) as executor:

        # Init a requests session
        with requests.Session() as session:

            session.mount('https://', HTTPAdapter(max_retries=0, pool_connections=workers, pool_maxsize=workers))
            # Set any session parameters here before calling `fetch`
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    function,
                    *(session, path, product, overwrite) # Allows us to pass in multiple arguments to `fetch`
                )
                for product in product_list
            ]

            # TODO: This makes it possible to get the functions return statement
            for response in await asyncio.gather(*tasks):

                export_list.append(response)
                pass


    return export_list


def read_products_data(path, product_list, overwrite=False):
    '''
    Reads the product data from disk given product_codes from a product_list

    Inputs
    -----------
    * path : str
        Path where to store the data

    * product_list : list
        List with all products

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    List with all path_urls
    '''
    products = []
    products_read = 0
    file_path = f"{path}/complete_product_list.pkl"

    # Read from disk
    if overwrite == False and utils.check_exists(file_path) == 1:
        
        # Try to read the data from disk
        products = utils.pickle_read(file_path, "Products_file")
        if products['status'] == 1:
            products = products['data']
            logging.info("Reading products file!")
            return products
        else:
            logging.info(f"Products error: {products['msg']} --> Trying to scrape data")


    # Go through every product in the product_list
    for product in product_list:

        # Get the product code
        product_code = string_parser.get_product_code(product)
        if product_code == "CODE_PARSING_ERROR":
            return "CODE_PARSING_ERROR"

        # File path
        file_path = f"{path}/products/{product_code}.pkl".format(product_code)

        try:
            # Read the product data from disk
            product_data = utils.pickle_read(file_path)

            if product_data['status'] == 1:
                products.append(product_data['data'])
                products_read += 1

        except Exception as e:
            logging.info(f"Reading product error: {e}")
            pass
    
    logging.info(f"Number of product_info read: {products_read}")
    utils.pickle_dump(file_path, products, "Products_file") 


    return products
    

def get_inventory(path, overwrite=False):
    '''
    Fetches the entire inventory from Willys

    Inputs
    -----------

    * path : str
        Path where to store the data

    * overwrite : boolean [default=False]
        Should the data be overwritten?

    Return
    ---------
    Database
    '''

    ipython_status = utils.run_from_ipython()
    path_dependencies = f"{path}/dependencies"
    database = None
    data_size = 10000

    # Init the folder struct
    utils.init_folder_struct(path)

    # Fetch the product list, list with all producs in their inventory
    product_list = get_product_list(path_dependencies, data_size, overwrite)
    utils.write_json("deb/test.json", product_list)

    # Asyncio loops are not supported in ipython!
    if ipython_status == False:
        
        # Number of asyncrioynous fetches
        workers = 1000 

        # Fetch info about every product
        loop = asyncio.get_event_loop()
        future = asyncio.ensure_future(get_data_asynchronous(path_dependencies, product_list, scraper.get_product_info, workers, overwrite))
        products_list = loop.run_until_complete(future)
        products = string_parser.parse_producs_status(path_dependencies, products_list)
    
    else:
        logging.info("To scrape product_data, run the script from the shell!")
        products = read_products_data(path_dependencies, product_list)

    # Create the EAN database
    database = database_func.create_ean_database(path, products)


    return database