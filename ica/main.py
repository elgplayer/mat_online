# -*- coding: utf-8 -*-

import os
import json
import pickle
import sys
import logging

import pandas as pd

from src import utils
from src import scraper
from src import database_lookup
from src import fetcher
from src import database


# Create logger
logger = utils.create_logger()

# Get the loggers name
logger = logging.getLogger(__name__)
logging.info("Started!")

# Creates the SKU database
database.create_sku_db()


logging.info("Finished!")