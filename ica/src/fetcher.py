# -*- coding: utf-8 -*-

import os
import json
import sys
import logging
import asyncio
from concurrent.futures import ThreadPoolExecutor
from requests.adapters import HTTPAdapter


import requests


from src import utils
from src import scraper


# Get the loggers name
logger = logging.getLogger(__name__)


async def get_data_asynchronous(product_list, function, parsed_store_data=None,
                                workers=1000, overwrite=False):
    '''
    Downloads pictures asynchronous

    Inputs
    -----------
    * path : str
        Path where to store the data

    * product_list : List
        Products to fetch

    * workers : int [default=1000]
        How many products should be fetched asynchronous?

    * overwrite : boolean [default=False]
        Should the data be overwritten?
    '''

    export_list = []

    # Start threadpool
    with ThreadPoolExecutor(max_workers=workers) as executor:

        # Init a requests session
        with requests.Session() as session:

            session.mount('https://', HTTPAdapter(
                max_retries=0, pool_connections=workers, pool_maxsize=workers))
            # Set any session parameters here before calling `fetch`
            loop = asyncio.get_event_loop()
            tasks = [
                loop.run_in_executor(
                    executor,
                    function,
                    *(session, product, parsed_store_data, overwrite)
                )
                for product in product_list
            ]

            # This makes it possible to get the functions return statement
            for response in await asyncio.gather(*tasks):
                export_list.append(response)
                pass

    return export_list


def fetch_products(session, products_url, parsed_store_data=None,
                   overwrite=False):
    '''
    Fetches products given a URL

    Inputs
    -----------
    * session : request.session
        Request session

    * products_url : str
        URL to the products to fetch

    * parsed_store_data : dict [default=None]
        Parsed store data dict**

    * overwrite : Boolean [default=False]
        Should data be overwritten?**

    Return
    ----------
    A list with the items parsed as JSON
    If error: response.status_code or ERROR
    '''
    response = session.get(products_url)
    return_var = None

    if response.status_code == 200:
        try:
            response_json = response.json()
            return_var = response_json
        except Exception as e:
            logging.warning(f"Error parsing data | Error {e}")
            return_var = "ERROR"
    else:
        return_var = response.status_code

    return return_var


def fetch_category(session, category, parsed_store_data, overwrite=False):
    '''
    Fetches all products for a specific category

    Inputs
    -----------
    * session : request.session
        Request session

    * category : str
        Category to the products to fetch

    * parsed_store_data : dict
        Parsed store data dict

    * overwrite : Boolean [default=False]
        Should data be overwritten?**

    Return
    ----------
    List with every product data
    '''

    api_limit = 6 # How many products to be fetched in each request
    products_workers = 6 # Number of asynchronous fetches

    base_url = parsed_store_data['base_url']
    product_url = parsed_store_data['product_url']
    products_url_list = []

    try:
        # Gets all products in a category
        category_url = '{}?categories={}&&bb=true'.format(
            base_url, category['seoUrl'])
        category_data = requests.get(category_url)
        category_data_json = category_data.json()
    except Exception as e:
        logging.warning(f"Category error: {e}")
        pass

    # Initializes the product item url
    product_item_url = '{}?productIds='.format(product_url)

    # Goes through all products in a category
    for kx, product in enumerate(category_data_json['items']):

        # Appends the product id to the product URL so multiple
        # products can be fetched at the same time
        product_id = product['id']
        product_item_url += "{},".format(product_id)

        # Sets a limit to the number of products to fetched in each request
        if kx % api_limit == 0:
            # Resets the URL
            products_url_list.append(product_item_url)
            product_item_url = '{}?productIds='.format(product_url)

    # Fetch info about every product
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    future = asyncio.ensure_future(get_data_asynchronous(
        products_url_list, fetch_products, workers=products_workers))
    data = loop.run_until_complete(future)

    # Flatten the list
    data = [item for sublist in data for item in sublist]
    logging.debug(f"Category: {category['seoUrl']} finished!")


    return data


def get_database(store, date, overwrite=False):
    '''
    Loads the database either from local file or by scraping it

    Input
    ----------
    * store : str
        What store to scrape
    
    * date : dict
        Dictionary with the dates

    * overwrite : Boolean [default=False]
        Should the data be overwritten?

    Return
    ----------
    Database (dict)

    '''
    
    # Get the parsed_store_data
    parsed_store_data = scraper.get_api_links(store)

    store_name = parsed_store_data['store_name']
    
    # Assigns the current path
    path = "output/{}/{}".format(date['year'], date['week'])
    
    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
        logging.info(f"Creating path: {path}")
    
    # Sets the file location
    file_location = '{}/{}.pkl'.format(path, store_name)
    
    # Download the database
    if not os.path.exists(file_location) or overwrite == True:
        
        database = scraper.download_ica_database(parsed_store_data)
        # Save the DB as a pickle file
        utils.write_pickle(file_location, database)
    
    # Load from file
    else:

        database = utils.load_pickle(file_location, f"{store_name} DB loaded!")
        if database['status'] == 1:
            database = database["data"]


    return database