import os
import json
import pickle
import sys
import logging

import pandas as pd

from src import utils
from src import scraper
from src import database_lookup
from src import fetcher
from src import database_stats


# Get the loggers name
logger = logging.getLogger(__name__)


def create_sku_db():
    '''
    Creates an SKU database (Dictionary where the key is the SKU number)
    Saves the database to disk.

    Return
    ----------
    SKU database
    '''

    # Load the stores file
    file_path = 'deb/stores.json'
    # Check that the file exists
    if not os.path.exists(file_path):
        logger.warning("NO API FILE FOUND")
        raise 'NO_API_FILE'
    # If the file exists, open the file
    else:    
        with open(file_path) as f:
            stores = json.load(f)

    # Check if python is run from an ipython interpreter
    ipython_status = utils.run_from_ipython()
    logging.info(f"iPython Status: {ipython_status}")
    if ipython_status == True:
        raise "Async error: iPython is not supported!"

    path_pkl = "output/sku_db.pkl"
    path_json= "output/sku_db.json"
    sku_db = {}

    # Get the date
    date = utils.get_date()

    for store in stores:

        db = get_database(store, date)

        for item in db:

            sku = item["id"]
            sku_db[sku] = item


    # Write to file
    utils.write_pickle(path_pkl, sku_db)
    utils.write_json(path_json, sku_db)


    return sku_db


def get_database(store, date):
    '''
    Gets the database for a store
    
    #TODO : CHECK FOR INVALID INPUT
    Inputs
    ----------
    * store : dict
        Dictionary with store info
    
    * date: dict:
        Dictionary with date information (used for path parsing)

    Return
    ----------
    Database for the given store (dict)
    '''
    # Get the database for each store
    db = fetcher.get_database(store, date, False)

    return db
    


