# -*- coding: utf-8 -*-

import json
import logging
import sys

import requests
import asyncio

from src import utils
from src import fetcher

# Get the loggers name
logger = logging.getLogger(__name__)


def get_api_links(store_data):
    '''
    Creates API url's with the stores id

    Input
    -----------
    * store_data : dict
        Dictionary with name and url_id

    Return
    -----------
    Dictionary with all URLS and the store name (dict)
    '''

    url_id = store_data['url_id']
    url_id_split = url_id.split('_')
    store_id = url_id_split[-1]
    # TODO: This does not use the store_name key in the store data?
    store_name = url_id_split[0].replace('-', '_')[:-3]

    export_data = {
        'api_url': "https://handla.ica.se/api/product-info/v1/store/{}/category/catalog80002".format(store_id),
        'base_url': "https://handla.ica.se/api/content/v1/collections/facets/customer-type/B2C/store/{}/products".format(url_id),
        'product_url': "https://handla.ica.se/api/content/v1/collections/customer-type/B2C/store/{}/products".format(url_id),
        'store_name': store_name
    }


    return export_data


def download_ica_database(parsed_store_data, workers=6):
    '''
    Downloads a ICA stores entire database

    Inputs
    ----------
    * parsed_store_data : dict
        Dictionary with all the API url's

    * workers : int [default=6]
        How many categories that should be fetched at the same time

    Return
    ----------
    A database (list) with all items in their entire inventory
    '''
    
    # Gets the product categories
    api_url = parsed_store_data['api_url']
    api_header = requests.get(api_url).text
    api_header_json = json.loads(api_header)
    product_categorys = api_header_json['childCategories']

    # Verbose
    store_name = parsed_store_data['store_name']
    logging.info(f"Starting store: {store_name}")

    # Start the async fetching of all product categories
    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(fetcher.get_data_asynchronous(
        product_categorys, fetcher.fetch_category, 
        parsed_store_data, workers=workers))
    data = loop.run_until_complete(future)

    # Flatten all the cateogories into a single large list
    database_flat = [item for sublist in data for item in sublist]


    return database_flat
