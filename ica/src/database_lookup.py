# -*- coding: utf-8 -*-
"""
Created on Sat Aug 10 14:20:49 2019

@author: Carl
"""

import os
import json
import pickle
import logging
import re
from collections import Counter
import datetime

import pandas as pd

from src import utils
from src import scraper

# Get the loggers name
logger = logging.getLogger(__name__)


def load_database(store_data):
    '''
    Loads the database either from local file or by scraping it

    Input
    ----------
    * store_data

    '''
    
    store_name = store_data['store_name']
    
    # Get the date
    date = utils.get_date()
    
    # Assigns the current path
    path = "output/{}/{}/{}".format(date['year'], date['week'], store_name)
    
    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
    
    # Sets the file location
    file_location = '{}/database.pkl'.format(path)
    
    if not os.path.exists(file_location):
        
        database = scraper.download_ica_database(store_data)
        logging.info("Scraping database")
        
        # Export the database to PICKLE
        utils.pickle_dump(database, "database", store_name)
    
    else:
        
        database = pickle.load(open(file_location, 'rb'))
        logging.info("Database loaded from file")
        

    return database


def find_item(database, sku_id):
    '''
    Loops through the list and returns the index of the item
    
    Attributes:
        
        database (list): Database to loop through
        sku_id (str): Numerical string that is a objects unique identifier (EAN number)
    
    return: Shallow copy of the item, else 'item_not_found' (str)
    '''
    
    # TODO: The database should instead be a dictionary where keys are the SKU number
    for k,x in enumerate(database):
        
        if database[k]['id'] == sku_id:
            
            return database[k]
        
    return 'ITEM_NOT_FOUND'