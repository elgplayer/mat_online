
import os
import json
import pickle
import logging
import re
from collections import Counter
import datetime

import pandas as pd



def nutrition_parser(item):
    '''
    Gets the nutrion value string and parses it, 
    returning a dict with the nutrional values
    
    Attributes:
        
        item (dict item): An item in the database dictionary
    
    return: nutrion_dict (dict) or 'no_nutrion_info' (str)
    '''
    
    if not 'nutritionalInfo' in item['product']:
        
        return 'no_nutrion_info'

    # Assign the value
    nutrion_info = item['product']['nutritionalInfo']
    nutrion_dict = {}
    
    # Replace action
    nutrion_info = nutrion_info.replace('Energi (kcal)', 'Energi_kcal')
    nutrion_info = nutrion_info.replace('Energi (kJ)', 'Energi_kJ')                                    
    nutrion_info = nutrion_info.replace('Varav mättat fett', 'Varav_mättat_fett')   
    nutrion_info = nutrion_info.replace('Varav socker', 'Varav_socker')                                    
    
    # Split the string
    nutrion_info = nutrion_info.split(', ')
    
    # Go through the splitted strings
    for k,x in enumerate(nutrion_info):
        
        # Place the strings in a dictionary
        split_str = nutrion_info[k].split(' ')
        nutrion_key = split_str[0]
        nutrion_value = float(split_str[1])        
        nutrion_dict[nutrion_key] = nutrion_value
        
        
    return nutrion_dict


def ingredient_parser(item):
    '''
    Gets all ingridents of the item
    
    Attributes:
        
        item(dict object): Item in database dictionary
        
    returns: ingredient_list (list) or 'no_ingredien_info' (str)
    
    '''
    
    if not 'ingredientInfo' in item['product']:
        
        return 'no_ingredien_info'

    ingredient_str = item['product']['ingredientInfo']
    
    # TODO: MANYYY Exceptionss
    #ingredient_str = ingredient_str.replace('.', '')
    #ingredient_list = re.split(',\s*(?![^()]*\))', ingredient_str)
    
    
    return ingredient_str


def price_parser(item):
    '''
    Gets the price of the item
    
    Attributes:
        
        item(dict object): Item in database dictionary
        
    returns: price_dict (dict) or 'no_price_info' (str)
    
    '''    
    
    if not 'price' in item:
        
        return 'no_price_info'    
    
    
    # Price dict
    price_dict = {'price_str': item['ordinaryPriceProps'],
                  'price_int': item['price'],
                  'discount': 0}
    
    # If discount
    if item['promoPriceProps'] != None:
        
        # OOF
        try:
            discount_dict = item['promoPriceProps']
            old_price = item['price']['comparePrice']
            
            new_price_str = discount_dict['comparePrice'].replace(',', '.')
            new_price = float(new_price_str.split(' ')[1])
            
            price_dict['discount_percentage'] = new_price / old_price
            price_dict['discount_percentage_str'] = str(round( (1 - price_dict['discount_percentage'])*100) ) + " %"
            price_dict['discount_sek'] = old_price - new_price
            price_dict['discount_sek_str'] = str(round(price_dict['discount_sek']))
            price_dict['discount'] = 1
            price_dict['discount_dict'] = discount_dict
            price_dict['list_price'] = item['price']['listPrice']
            price_dict['list_price_sale'] = (item['promoPriceProps']['price']['displayName'])
            price_dict['productCount'] =  item['promoPriceProps']['productCount']
            
        except Exception as e:
            logging.debug("Price parser error: {}".format(e))
            pass
            
    return price_dict


def product_info_parser(item):
    '''
    Gets product info from the item
 
    Attributes:
        
        item(dict object): Item in database dictionary
        
    returns: product_dict_export (dictionary) or 'no_product_info' (str)
    '''
    
    if not 'product' in item:
        
        return 'no_product_info'
    
    product_dict = item['product']
    
    categories = []
    for k,x in enumerate(product_dict['categories']):
        categories.append(product_dict['categories'][k]['name'])
    
    # Sets most categories to None to aviod key-error    
    origin_country = None
    if 'originCountryCode' in item:
        origin_country = product_dict['originCountryCode']
    
    compare_price = None
    if 'comparPriceCode' in product_dict:
        compare_price = product_dict['comparPriceCode']
    
    uom_code = None
    if 'uomCode' in product_dict:
        uom_code = product_dict['uomCode']
        
    brand = None
    if 'brand' in product_dict:
        brand = product_dict['brand']
        
    netWeightVolume = None
    if 'netWeightVolume' in product_dict:
        netWeightVolume = product_dict['netWeightVolume']
 
    meanWeight = None
    if 'meanWeight' in product_dict:
        meanWeight = product_dict['meanWeight']
        
    longDescription = None
    if 'longDescription' in product_dict:
        longDescription = product_dict['longDescription']
    
    # Product information
    product_dict_export = {
            
            'brand': brand,
           'sku': product_dict['sku'],
           'categories': categories,
           'comparPriceCode': compare_price,
           'name': product_dict['name'], 
           'order_unit': product_dict['orderUnits'],
           'orgin_country': origin_country,
           'refrigerationEligible': product_dict['refrigerationEligible'],
           'netWeightVolume': netWeightVolume,
           'meanWeight': meanWeight,
           'longDescription': longDescription,
           'sales_unit': product_dict['salesUnit'],
           'slug': product_dict['slug'],
           'type': product_dict['type'],
           'storeId': product_dict['storeId'],
           'uomCode': uom_code
                           
           }


    return product_dict_export


def sort_discount(database, parsed_store_data):
    '''
    Sorts the list with all items that have discount and prints the items 
    with the largest fractional discount and largest discount in SEK.
    
    It also calls functions that export the discount list to CSV, JSON, 
    Pickle and HTML
    
    Attributes:
        
        database (list): Database list with all items
        parsed_store_data (dict): Dictionary with store_name
        
    '''
    
    # TODO: Add criteraias, google sheet export
    items_with_discount_sek = []
    items_with_discount = []
    
    store_name = parsed_store_data['store_name']
    
    # Appends items that have discount to a discount list
    for kx,ky in enumerate(database):
        
        item = database[kx]
        price_dict = price_parser(item)
        
        if price_dict['discount'] == 1:
            
            #if item['product']['type'] == 'food':
                
            price_dict['item'] = database[kx]
            price_dict['database_key'] = kx
            items_with_discount_sek.append(price_dict)
            items_with_discount.append(price_dict)
                
    # Sort after biggest discount in % 
    items_with_discount_sek = sorted(items_with_discount_sek, 
                                     key=lambda k: k['discount_sek'], 
                                     reverse=True)
    
    # Sort after biggest in SEK
    items_with_discount = sorted(items_with_discount_sek, 
                                 key=lambda k: k['discount_percentage'])        
    
    def get_biggest_brand():
        '''
        Gets the biggest brand and how many entries of that brand
        
        return: Dictionary with largest brand and how many (dict)
        '''
        
        # Appends the diffrent brands to "brands"
        brands = []
        for item in (items_with_discount):
            product_info = product_info_parser(item['item'])
            if product_info != 'no_product_info':
                brands.append(product_info['brand'])
        
        # Get largest brand
        brands = list(filter(None, brands))        
        brands_dict = dict(Counter(brands))
        largest_brand_nr = 0
    
        # Goes through the list setting the largest brand
        for k, x in enumerate(brands_dict):
           if brands_dict[x] > largest_brand_nr :
               largest_brand_nr = brands_dict[x]
               largest_brand = x
               
               
        return {'largest_brand': largest_brand, 
                'nr_of_entries': largest_brand_nr}
    
    def get_item_name(item):
        return item['product']['name']
    
    def print_discount_info():
        # OMFG
        largest_sek = items_with_discount_sek[0]['discount_sek_str']
        largest_percentage = items_with_discount[0]['discount_percentage_str']
        largest_sek_percentage = items_with_discount_sek[0]['discount_percentage_str']
        largest_percentage_sek = items_with_discount[0]['discount_sek_str']
            
        biggest_brand = get_biggest_brand()    
        
        # Printy print
        print("\n## Discount Info ##")
        print("Items with discount: {}\nLargest brand: {} ({} entries)".format(
                
                len(items_with_discount), 
                biggest_brand['largest_brand'], 
                biggest_brand['nr_of_entries'])
        )
        
        print("Largest SEK discount: {} kr ({})| Item name: {}".format(
                
                largest_sek, 
                largest_sek_percentage, 
                get_item_name(items_with_discount_sek[0]['item']))
        
        )        
        
        print("Largest % discount: {} ({} kr)| Item name: {}".format(
                
                largest_percentage, 
                largest_percentage_sek, 
                get_item_name(items_with_discount[0]['item']))
                
                )        


    #####################
    ### EXPORT FIESTA ###
    #####################
    
    # Check if the list is empty
    if items_with_discount != []:
    
        # CSV    
        export_to_csv(items_with_discount_sek, "sek", store_name)
        export_to_csv(items_with_discount, "percentage", store_name)
        
        # JSON
        export_to_json(items_with_discount, "percentage", store_name)
        export_to_json(items_with_discount_sek, "sek", store_name)
        
        # PKL
        export_to_pickle(items_with_discount, "percentage", store_name)
        export_to_pickle(items_with_discount_sek, "sek", store_name)
        
        # HTML 
        export_to_html(items_with_discount, "percentage", store_name)
        export_to_html(items_with_discount_sek, "sek", store_name)
        
        # Discount info
        print_discount_info()       
    
    else:
    
        logging.info("No items with discount!")
    
    
def export_to_csv(discount_list, file_name, store_name):
    '''
    Export the data to CSV format
    
    Attributes:
        
        discount_list (list): List with items that will be exported
        file_name (str): What the exported list should be named
    
    return: None, Writes to disk instead
    '''
    
    # Gest the current date
    date = get_date()
    
    # Header
    header = "Discount|Name|List_Price|Sale_List_Price|Sale_Price_CMP|Normal_Price_CMP|Brand|Mean_Weight|Categories|Valid_Until|SKU\n"
    
    # Assigns the current path
    path = "output/{}/{}/{}".format(date['year'], date['week'], store_name)
    
    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
    
    # Sets the file location
    file_location = '{}/{}.csv'.format(path, file_name)

    with open(file_location, 'w', encoding='utf-8') as f:
        
        # Write header
        f.write(header)
        
        # Go through the list
        for line in discount_list:
            
            # Sort Format
            if file_name == "sek":
                
                sort_format = "{} sek ({})".format(
                        
                        line['discount_sek_str'], 
                        line['discount_percentage_str']
                        
                        )
            else:
                
                sort_format = "{} ({} sek)".format(
                        
                        line['discount_percentage_str'], 
                        line['discount_sek_str']
                        
                        )
            
            product_info =  product_info_parser(line['item'])
            
            # Line
            list_price = line['price_str']['price']
            sale_list_price = line['item']['promoPriceProps']['price']['displayName']   
            sale_price_cmp = line['discount_dict']['comparePrice']
            normal_price_cmp = line['item']['ordinaryPriceProps']['comparePrice']
            validUntil = line['item']['promoPriceProps']['validUntil']
            sku = line['item']['id']
            
            # Product Info
            name = product_info['name']
            brand = product_info['brand']
            mean_weight =  product_info['netWeightVolume']
            uom_code = product_info['uomCode']
            categories =  product_info['categories']
            
            
            # Line to write
            line_print = sort_format + "|{}|{}|{}|{}|{}|{}|{} ({})|{}|{}|{}\n"
            
            line_print = line_print.format(name, list_price, 
                                           sale_list_price, sale_price_cmp, 
                                           normal_price_cmp, brand, 
                                           mean_weight, uom_code, 
                                           categories, validUntil, sku)
            
            f.write(line_print) # Writes the line to file

    logging.info("{} CSV exported".format(file_name))
    
    
def export_to_json(discount_list, file_name, store_name):
    '''
    Export the data to JSON format
    
    Attributes:
        
        discount_list (list): List with items that will be exported
        file_name (str): What the exported list should be named
    
    return: None, Writes to disk instead
    '''

    # Get the date
    date = get_date()
    
    # Assigns the current path
    path = "output/{}/{}/{}".format(date['year'], date['week'], store_name)
    
    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
    
    # Sets the file location
    file_location = '{}/{}.json'.format(path, file_name)

    # Opens file object
    with open(file_location, 'w', encoding='utf-8') as f:
        
        # Export list
        export_list = []
        
        # Go through the list
        for line in discount_list:
            
            # Sort Format
            if file_name == "sek":
                sort_format = "{} sek ({})".format(line['discount_sek_str'], 
                                           line['discount_percentage_str'])
            else:
                sort_format = "{} ({} sek)".format(line['discount_percentage_str'], 
                                                   line['discount_sek_str'])
            
            # Gets product info
            product_info =  product_info_parser(line['item'])
            
            # Line
            list_price = line['price_str']['price']
            sale_list_price = line['item']['promoPriceProps']['price']['displayName']      
            sale_price_cmp = line['discount_dict']['comparePrice']
            normal_price_cmp = line['item']['ordinaryPriceProps']['comparePrice']
            validUntil = line['item']['promoPriceProps']['validUntil']
            sku = line['item']['id']
            
            # Product Info
            name = product_info['name']
            brand = product_info['brand']
            mean_weight =  product_info['netWeightVolume']
            uom_code = product_info['uomCode']
            categories =  product_info['categories']
            
            # Export dictionary
            export_dict = {'name': name,
                           'sale_info': sort_format,
                           'sale_price_cmp': sale_price_cmp, 
                           'normal_price_cmp': normal_price_cmp,
                           'normal_list_price': list_price,
                           'sale_list_price': sale_list_price,
                           'mean_weight': mean_weight,
                           'uom_code': uom_code,
                           'brand': brand, 
                           'categories': categories, 
                           'validUnitil': validUntil, 
                           'sku': sku}
            
            # Append the export dictionary to a list
            export_list.append(export_dict)
        
        # Save the list as JSON
        json.dump(export_list, f, indent=4, ensure_ascii=False)
        logging.info("JSON exported")
    
    
def export_to_pickle(discount_list, file_name, store_name):
    '''
    Export the data to PKL format
    
    Attributes:
        
        discount_list (list): List with items that will be exported
        file_name (str): What the exported list should be named
    
    return: None, Writes to disk instead
    '''
    
    # Get the date
    date = get_date()
    
    # Assigns the current path
    path = "output/{}/{}/{}".format(date['year'], date['week'], store_name)
    
    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
        logging.debug("Created path: {}".format(path))
    
    # Sets the file location
    file_location = '{}/{}.pkl'.format(path, file_name)
    
    # Save it to disk
    pickle.dump(discount_list, open(file_location, 'wb'))
    logging.info("{} PICKLE dumped".format(file_name))


def export_to_html(discount_list, file_name, store_name):
    '''
    Export the data to HTML format
    
    Attributes:
        
        discount_list (list): List with items that will be exported
        file_name (str): What the exported list should be named
    
    return: None, Writes to disk instead
    '''        
    # Get the date
    date = get_date()
    
    # Assigns the current path
    path = "output/{}/{}/{}".format(date['year'], date['week'], store_name)
    
    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
        logging.debug("Created path: {}".format(path))
    
    # Sets the file location
    csv_file_location = '{}/{}.csv'.format(path, file_name)
    html_file_location = '{}/{}.html'.format(path, file_name)
    
    # Export the CSV file if the file does not exists
    if not os.path.exists(csv_file_location):
        
        # Export the CSV file
        export_to_csv(discount_list, file_name)
   
    # Read the CSV file
    df = pd.read_csv(csv_file_location, sep='|', encoding='utf-8')

    # Output to HTML
    df.to_html(html_file_location)
    logging.info("{} HTML exported".format(file_name))


def create_sku_database():
    '''
    Creates the SKU database
    '''
    # Get the date
    date = get_date()
    
    # Assigns the current path
    path = "output/{}/{}".format(date['year'], date['week'])
    file_location = '{}/sku_database.pkl'.format(path)
    
    # Create the folders if not exists
    if not os.path.exists(path):
        logging.debug("Created path: {}".format(path))
        os.makedirs(path, exist_ok=True)
        

    sku_database = {'items': {}, 'database_info': 
                                {'week': date['week'], 'year': date['year']}}
    
    pickle.dump(sku_database, open(file_location, 'wb'))
    
    logging.info("SKU database created")


def append_sku(database):
    '''
    Appends items to the SKU database   
    ''' 
    # Get the date
    date = get_date()
    
    counter = 0
    
    # Assigns the current path
    path = "output/{}/{}".format(date['year'], date['week'])
    file_location = '{}/sku_database.pkl'.format(path)

    path_final = "output/".format(date['year'], date['week'])
    file_location_final = '{}/sku_database.pkl'.format(path_final)
    
    # Load the datbase from file
    sku_database = pickle.load(open(file_location, 'rb'))
    logging.debug("SKU database loaded")
    
    # Go through the database
    for item in database:
        
        product = product_info_parser(item)
        item_sku = product['sku']
        
        if item_sku not in sku_database['items']:
            
            counter += 1
            
            item_dict = {'product': product,
                         'ingriedent': ingredient_parser(item),
                         'nutrion': nutrition_parser(item)}
            
            sku_database['items'][item_sku] = item_dict

            
    # Save the database
    pickle.dump(sku_database, open(file_location, 'wb'))
    pickle.dump(sku_database, open(file_location_final, 'wb'))
    logging.info("SKU database pickled | {} items added".format(counter))
