# -*- coding: utf-8 -*-

import os
import json
import pickle
import sys
import logging

import requests
import pandas as pd

from src import utils
from src import fetcher
from src import scraper

# Create logger
logger = utils.create_logger()

# Get the loggers name
logger = logging.getLogger(__name__)




# Get the date
date = fetcher.get_date()
# Assigns the current path
path = "../output/{}/{}".format(date['year'], date['week'])


db = fetcher.get_database(path, True)




# path = '../deb'
# file_path = '{}/stores.json'.format(path)

# # Create directories if the path does not exists
# if not os.path.exists(path):
#     os.makedirs(path, exist_ok=True)

# # Check that the file exists
# if not os.path.exists(file_path):
#     logging.CRITICAL("NO API FILE FOUND")
#     raise 'NO_API_FILE'

# # If the file exists, open the file
# else:    
#     with open(file_path) as f:
#         stores = json.load(f)
        
# database = load_database()
    
# # Loop over the diffrent stores
# for store_info in stores:
    
#     print(store_info)
    
#     discount_data = get_discount(store_info)