# -*- coding: utf-8 -*-
import json
import logging

import requests

# Get the loggers name
logger = logging.getLogger(__name__)

def download_hemkop_database():
    '''
    Downloads Hemköps database
    
    '''
    
    # Giant database
    giant_database = []
    
    categories_url = "https://www.hemkop.se/leftMenu/categorytree"
    categories = requests.get(categories_url).text
    categories_json = json.loads(categories)
    
    # Goes through the categories
    for k,x in enumerate(categories_json['children']):
        
        print("Category {}/{}".format(k+1, len(categories_json['children'])))
        
        # Get the URL for the category
        category = categories_json['children'][k]
        category_url = category['url']
        
        # Get the products in the category
        product_url = "https://www.hemkop.se/c/{}?categoryPath={}&size=7000".format(category_url, category_url)
        product_data = requests.get(product_url).text
        product_data_json = json.loads(product_data)
        
        # Check for error CODE
        if 'errorCode' not in product_data_json:
        
            # If the category actually has items
            if product_data_json['pagination']['totalNumberOfResults'] > 0:
                
                # Goes through all the items from the category
                for kx,ky in enumerate(product_data_json['results']):
                    
                    # Append to the large database
                    giant_database.append(product_data_json['results'][k])
        
            else:
                
                # Verbose
                print("No results: {}".format(category_url))
        
        else:
            
            logging.debug("ERROR CODE product_data_json: {}".format(
                    product_data_json['errorCode']))
            
    return giant_database


def get_discount(store_info):
    
    
    discount_url = "https://www.hemkop.se/search/campaigns/mix?anonymous=true&q={}&size=7000".format(store_info['url_id'])
    discount_data = requests.get(discount_url).text
    discount_data_json = json.loads(discount_data)
    
    try:
        
        return discount_data_json['results']
    
    except:
        
        return 'no_discount'
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    