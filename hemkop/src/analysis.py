

def price_parser(item):
    '''
    Gets the price of the item
    
    Attributes:
        
        item(dict object): Item in database dictionary
        
    returns: price_dict (dict) or 'no_price_info' (str)
    
    '''    
    
    if not 'price' in item:
        return 'no_price_info'    
    
    # Price dict
    price_dict = {'price_str': item['ordinaryPriceProps'],
                  'price_int': item['price'],
                  'discount': 0}
    
    # If discount
    if item['promoPriceProps'] != None:
        
        # OOF
        try:
            discount_dict = item['promoPriceProps']
            old_price = item['price']['comparePrice']
            
            new_price_str = discount_dict['comparePrice'].replace(',', '.')
            new_price = float(new_price_str.split(' ')[1])
            
            price_dict['discount_percentage'] = new_price / old_price
            price_dict['discount_percentage_str'] = str(round( (1 - price_dict['discount_percentage'])*100) ) + " %"
            price_dict['discount_sek'] = old_price - new_price
            price_dict['discount_sek_str'] = str(round(price_dict['discount_sek']))
            price_dict['discount'] = 1
            price_dict['discount_dict'] = discount_dict
            price_dict['list_price'] = item['price']['listPrice']
            price_dict['list_price_sale'] = (item['promoPriceProps']['price']['displayName'])
            price_dict['productCount'] =  item['promoPriceProps']['productCount']
            
        except Exception as e:
            logging.debug("Price parser error: {}".format(e))
            pass
            
    return price_dict