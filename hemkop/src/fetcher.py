# -*- coding: utf-8 -*-

import os
import json
import pickle
import sys
import logging
import datetime

import requests
import pandas as pd

from src import scraper


# Get the loggers name
logger = logging.getLogger(__name__)


def get_date():
    '''
    Gets the current week and year
    
    return: Current year and week (dict)
    '''
    date_now = datetime.datetime.now()
    year = date_now.strftime("%Y")    
    week = date_now.strftime("%W")    
    export_dict = {
        'year': year, 
        'week': week
    }

    return export_dict





def get_database(path, overwrite=False):
    '''
    Loads the database either from local file or by scraping it
    '''

    # Create the folders if not exists
    if not os.path.exists(path):
        os.makedirs(path, exist_ok=True)
    
    # Sets the file location
    # TODO: Dont asssume file exitension!
    file_location = '{}/database.pkl'.format(path)
    
    # Check if the file exists
    if not os.path.exists(file_location):
        logging.info("Scraping database")
        database = scraper.download_hemkop_database()
        logging.info("Database downloaded")
    
    else: 
        try:
            database = pickle.load(open(file_location, 'rb'))
            logging.info("Database loaded from file")
        except Exception as e:
            logging.error(f"Failed to open Pickled Database! | Error: {e}")
            return "ERROR"
        

    return database
